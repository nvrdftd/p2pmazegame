package main.java;

public class DummyTracker {
    private A a;
    DummyTracker(A a){
        this.a = a;
    }
    void setA(A a){
        this.a = a;
    }
    A getA(){
        return this.a;
    }
    public static void main(String[] args){
        DummyTracker d = new DummyTracker(new A(2));
        System.out.println(d.getA().getA());
        A a  = d.getA();
        a.setA(3);
        System.out.println(d.getA().getA());

    }

}

class A{
    private int a;
    A(int a){
        this.a = a;
    }
    void setA(int a){
        this.a = a;
    }
    int getA(){
        return this.a;
    }
}