package main.java;

public enum  PeerStatus {
    PRIMARY("PrimaryServer"),
    BACKUP("BackUpServer"),
    NORMAL("Normal"),
    DEAD("Dead");

    private  String status;
    PeerStatus(String status){
        this.status = status;
    }

}
