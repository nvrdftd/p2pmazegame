package main.java;

import java.io.Serializable;

interface IServerReply extends Serializable {
    class AuthReply implements IServerReply{
        private final boolean accepted;
        private final String playerId;
        private final String serverAuth;
        private final GameState gameState;
        private final ServerInternals serverInternals;
        /* If the backup server dies, the primary server can ask on of the servers
           to be the backup.
         */
        private final boolean becomeBackup;

        public AuthReply(boolean accepted,
                         String playerId,
                         String serverAuth,
                         GameState gameState,
                         ServerInternals serverInternals,
                         boolean becomeBackup) {
            this.accepted = accepted;
            this.playerId = playerId;
            this.serverAuth = serverAuth;
            this.serverInternals = serverInternals;
            this.becomeBackup = becomeBackup;
            this.gameState = gameState;
        }

        public static AuthReply decline() {
            return new AuthReply(false, null, null, null, null, false);
        }

        public boolean isAccepted() {
            return this.accepted;
        }

        public boolean isBecomeBackup() {
            return this.becomeBackup;
        }

        public String getPlayerId() {
            return this.playerId;
        }

        public String getServerAuth() {
            return this.serverAuth;
        }

        public GameState getGameState() {
            return this.gameState;
        }

        public ServerInternals getServerInternals(){ return this.serverInternals; }
    }

    class PingReply implements IServerReply {
        public enum PromotionStatus {
            NONE,
            PROMOTED_TO_PRIMARY,
            PROMOTED_TO_BACKUP
        }

        private final PromotionStatus promotionStatus;

        private final GameState gameState;

        private final ServerInternals serverInternals;

        /**
         * Server secret should be null if promotionStatus is neither
         * PROMOTED_TO_PRIMARY or PROMOTED_TO_BACKUP.
         */
        public PingReply(PromotionStatus promotionStatus,
                         GameState gameState,
                         ServerInternals serverInternals) {

            this.promotionStatus = promotionStatus;
            this.gameState = gameState;
            this.serverInternals = serverInternals;
        }

        public PromotionStatus getPromotionStatus() {
            return this.promotionStatus;
        }

        public GameState getGameState() {
            return this.gameState;
        }

        public ServerInternals getServerInternals() {
            return this.serverInternals;
        }

        public static PingReply createUpdate(GameState gameState) {
            return new PingReply(PromotionStatus.NONE, gameState, null);
        }

        public static PingReply createPromoteToBackup(GameState gameState, ServerInternals serverInternals) {
            return new PingReply(PromotionStatus.PROMOTED_TO_BACKUP, gameState, serverInternals);
        }

        public static PingReply createPromoteToPrimary(GameState gameState, ServerInternals serverInternals) {
            return new PingReply(PromotionStatus.PROMOTED_TO_PRIMARY, gameState, serverInternals);
        }

    }

    class MoveReply extends IServerReply.PingReply {

        private final boolean illegalMove;

        public MoveReply(PromotionStatus promotionStatus,
                         GameState gameState,
                         ServerInternals serverInternals,
                         boolean illegalMove) {

            super(promotionStatus, gameState, serverInternals);
            this.illegalMove = illegalMove;
        }

        public boolean isIllegalMove() {
            return illegalMove;
        }

        public static MoveReply createIllegal(GameState gameState) {
            return createReply(gameState, true);
        }

        public static MoveReply createReply(GameState gameState, boolean illegalMove) {
            return new MoveReply(PromotionStatus.NONE, gameState, null, illegalMove);
        }

        public static MoveReply createPromoteToBackup(GameState gameState, ServerInternals serverInternals, boolean illegalMove) {
            return new MoveReply(PromotionStatus.PROMOTED_TO_BACKUP, gameState, serverInternals, illegalMove);
        }
    }

}
