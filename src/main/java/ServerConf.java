package main.java;

import java.io.Serializable;

public class ServerConf implements Serializable {

    /* This class represents Server Configuration that shall be used by the players
       to connect to the server.
     */
    private Host primaryServerHost;
    private Host backupServerHost;
    private IPlayer primaryServer = null;
    private IPlayer backupServer = null;
    private String primaryId;
    private String backupId;

    public Host getPrimaryServerHost() {
        return this.primaryServerHost;
    }

    public Host getBackupServerHost() {
        return this.backupServerHost;
    }

    public IPlayer getPrimaryServer() {
        return this.primaryServer;
    }

    public IPlayer getBackupServer() {
        return this.backupServer;
    }

    public String getPrimaryId() {
        return this.primaryId;
    }

    public String getBackupId() {
        return this.backupId;
    }

    public void setPrimaryServer(IPlayer primaryServer, String primaryId) {
        this.primaryServer = primaryServer;
        this.primaryId = primaryId;
    }

    public void setBackupServer(IPlayer backupServer, String backupId) {
        this.backupServer = backupServer;
        this.backupId = backupId;
    }

    /*public static void main(String[] args) {
        System.out.println("Testing server conf");
        ServerConf serverConf = new ServerConf();
        try {
            serverConf.setPrimaryServer(new PeerPlayer(new Host("2", 3), "AA"), "1");
            System.out.println(serverConf.getPrimaryId());
            System.out.println(serverConf.getPrimaryServer().isServer());
        } catch (Exception e) {
            System.out.println("Something is wrong: " + e.toString());
        }
    }*/
}
