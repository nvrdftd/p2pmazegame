package main.java;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

interface ITrackerResp extends Serializable {
    class ConnResp implements ITrackerResp {
        public enum PlayerRole{
            SERVER,
            BACKUP,
            NORMAL
        };
        private String connectionId;
        private boolean reject;
        private String transactionId;
        private String rejectReason;
        private PlayerRole playerRole;
        private int hostPort;

        public ConnResp(String connectionId,
                         String transactionId,
                         int hostPort,
                         boolean reject,
                         String rejectReason){

            this.connectionId = connectionId;
            this.reject = reject;
            this.transactionId = transactionId;
            this.rejectReason = rejectReason;
            this.hostPort = hostPort;
        }

        public static ConnResp rejectResp(String transactionId,
                                          String rejectReason){
            return new ConnResp(null,
                                 transactionId,
                                -1,
                                true,
                                 rejectReason);
        }

        public void setPlayerRole(PlayerRole playerRole){
            this.playerRole = playerRole;
        }

        public PlayerRole getPlayerRole() {
            return playerRole;
        }

        public boolean isRejected(){
            return this.reject;
        }

        public String getConnectionId(){
            return this.connectionId;
        }

        public String getTransactionId(){
            return this.transactionId;
        }

        public String getRejectReason() {
            return this.rejectReason;
        }

        public int getHostPort(){
            return this.hostPort;
        }

        public String toString(){
            String ret = "ConnectionId: " + this.connectionId;
            ret += "TransactionId: " + this.transactionId;
            ret += "Reject: " + reject;
            return ret;
        }

    }

    class AnnouceResp implements  ITrackerResp {

        private String connectionId;
        private boolean reject;
        private String transactionId;
        private Map trackerInfos;
        private String rejectReason;

        public AnnouceResp(String connectionId,
                            String transactionId,
                            Map playerInfos,
                            boolean reject,
                            String rejectReason){
            this.connectionId = connectionId;
            this.reject = reject;
            this.transactionId = transactionId;
            this.rejectReason = rejectReason;
            this.trackerInfos = playerInfos;
        }

        public static AnnouceResp rejectResp(String transactionId,
                                             String rejectReason){
            return new AnnouceResp(null,
                                    transactionId,
                                    null,
                                    true,
                                    rejectReason);
        }

        public boolean isRejected(){
            return this.reject;
        }

        public String getConnectionId(){
            return this.connectionId;
        }

        public String getTransactionId(){
            return this.transactionId;
        }

        public String getRejectReason() {
            return this.rejectReason;
        }

        public Map getPlayerInfos() {
            return trackerInfos;
        }

        public String toString(){
            String ret = "ConnectionId: " + this.connectionId;
            ret += "TransactionId: " + this.transactionId;
            ret += "Reject: " + reject;
            return ret + trackerInfos.toString();
        }
    }

    class ScrapeResp implements  ITrackerResp {
        private String connectionId;
        private boolean reject;
        private String transactionId;
        private int scrapeInfos;
        private String rejectReason;

        public ScrapeResp(String connectionId,
                           String transactionId,
                           int scrapeInfos,
                           boolean reject,
                           String rejectReason){
            this.connectionId = connectionId;
            this.reject = reject;
            this.transactionId = transactionId;
            this.rejectReason = rejectReason;
            this.scrapeInfos = scrapeInfos;
        }

        public boolean isRejected(){
            return this.reject;
        }

        public String getConnectionId(){
            return this.connectionId;
        }

        public String getTransactionId(){
            return this.transactionId;
        }

        public String getRejectReason() {
            return this.rejectReason;
        }

        public int getScrapeInfos() {
            return scrapeInfos;
        }

        public String toString(){
            String ret = "ConnectionId: " + this.connectionId;
            ret += "TransactionId: " + this.transactionId;
            ret += "Reject: " + reject;
            ret += "Reject reason: " + this.rejectReason;
            return  ret + "\n" + "Num players: " + scrapeInfos;
        }

    }

    class FlushResp implements  ITrackerResp {
        private String connectionId;
        private boolean reject;
        private String transactionId;
        private String rejectReason;

        public FlushResp(String connectionId,
                           String transactionId,
                           boolean reject,
                           String rejectReason){
            this.connectionId = connectionId;
            this.reject = reject;
            this.transactionId = transactionId;
            this.rejectReason = rejectReason;
        }

        public boolean isRejected(){
            return this.reject;
        }

        public String getConnectionId(){
            return this.connectionId;
        }

        public String getTransactionId(){
            return this.transactionId;
        }

        public String getRejectReason() {
            return this.rejectReason;
        }

        public String toString(){
            String ret = "ConnectionId: " + this.connectionId;
            ret += "TransactionId: " + this.transactionId;
            ret += "Reject: " + reject;
            return  ret;
        }
    }

    class PingResp implements ITrackerResp{
        private String connectionId;
        private boolean reject;
        private String transactionId;
        private String rejectReason;

        public PingResp(String connectionId,
                         String transactionId,
                         boolean reject,
                         String rejectReason){
            this.connectionId = connectionId;
            this.reject = reject;
            this.transactionId = transactionId;
            this.rejectReason = rejectReason;
        }

        public boolean isRejected(){
            return this.reject;
        }

        public String getConnectionId(){
            return this.connectionId;
        }

        public String getTransactionId(){
            return this.transactionId;
        }

        public String getRejectReason() {
            return this.rejectReason;
        }

        public String toString(){
            String ret = "ConnectionId: " + this.connectionId;
            ret += "TransactionId: " + this.transactionId;
            ret += "Reject: " + reject;
            return  ret;
        }

    }

}
