package main.java;

public class InheritingPrimaryServer extends PrimaryServer {
    public InheritingPrimaryServer(GameState gameState,
                                   ServerInternals serverInternals,
                                   String playerId,
                                   IPlayer player,
                                   String serverAuth) {
        this.gameState = gameState;
        this.serverInternals = serverInternals;
        this.playerId = playerId;
        this.player = player;
        this.serverAuth = serverAuth;
    }

    public void start() {
        startAliveCheckerTimerTask();
        treasureRefillerTask();
    }

    public IServerReply.AuthReply callPrimaryJoin(IPlayer player) {
        // No longer accepts joining
        return IServerReply.AuthReply.decline();
    }

    public static void main(String[] args){

    }
}
