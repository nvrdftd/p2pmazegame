package main.java;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public abstract class PrimaryServer extends Server {
    protected Map<IPlayer, Long> playerLastAccessTime = new HashMap<IPlayer, Long>();
    protected boolean shouldPromoteBackUp;
    protected IPlayer player;

    public void start(){
        ;
    }

    /*This function updates the backup server when any gamestate in server.*/
    protected boolean updateBackup() {
        boolean success = true;
        synchronized (gameStateLock) {
            IPlayer backUpServer = gameState.getServerconf().getBackupServer();
            try {
                backUpServer.callUpdateOnBackUp(this.gameState, this.serverInternals);
                success = true;
            } catch (Exception e) {
                /* Problem with backUp, so something */
                //System.out.println(gameState.getServerconf().getPrimaryId());
                //System.out.println(gameState.getServerconf().getBackupId());
                setNotRunning(gameState.getServerconf().getBackupId());
                shouldPromoteBackUp = true;
                //System.out.println("BackUp cannot be reached");
                success = false;
            }
        }
        return success;
    }

    /**
     * startBackgroundAliveTimerTask() --> Pings all the players every few seconds to check the health of the
     * players.
     */
    protected void startAliveCheckerTimerTask() {
        final Timer timer = new Timer();
        final TimerTask task = new TimerTask() {
            @Override
            public void run() {
                checkAliveStatusOfPlayers();
            }
        };
        //Change the timer ping intervals.
        timer.scheduleAtFixedRate(task, 0, PING_INTERVAL);
        //System.out.println("Alive Checker Time Task Started");
    }

    protected void checkAliveStatusOfPlayers(){
        //System.out.println("Check ping status of players");
        for (PlayerInfo playerInfo: gameState.getPlayersList()) {
            if (!playerInfo.isRunning()) {
                return;
            }

            IPlayer player = serverInternals.getPeers().get(playerInfo.getId());
            Long lastAccessMillis = playerLastAccessTime.get(player);
            if (lastAccessMillis == null) {
                return;
            }

            long silentPeriod = System.currentTimeMillis() - lastAccessMillis;
            if (silentPeriod > 2 * PING_INTERVAL) {
                if (isSelf(playerInfo.getId())) {
                    // peer is primary server. ignoring.
                } else if (playerInfo.getId().equals(gameState.getServerconf().getBackupId())) {
                    if (!updateBackup()) {
                        //System.out.println("Backup not running");
                        setNotRunning(playerInfo.getId());
                        gameState.removePlayerFromMaze(playerInfo);

                    }
                    try {
                        //System.out.println("Removing player");
                        this.player.callTrackerToRemovePlayer(playerInfo.getId());
                    } catch (Exception e){
                        //System.out.println("Failed tracker" + e.getMessage());
                    }
                } else {
                    setNotRunning(playerInfo.getId());
                    gameState.removePlayerFromMaze(playerInfo);
                    //System.out.println("Removing player");
                    try {
                        this.player.callTrackerToRemovePlayer(playerInfo.getId());
                    } catch (Exception e){
                        //System.out.println("Failed tracker" + e.getMessage());
                    }
                }
            }
        }
    }

    /**
     *Method which takes care of updating the last access time of
     *the player to the server.
     */
    protected  void updateLatestAccessTime(IPlayer player) {
        this.playerLastAccessTime.put(player, System.currentTimeMillis());
    }

    /**
     *This function is used to Ping the primary server from time to time,
     * to check whether the server is alive, by doing this even the server
     * might know whether a ping request haven't yet arrived from a customer
     * since a very long time.
     */
    protected IServerReply.PingReply callPrimaryPing(IPlayer player, String playerId, String authCode) {
        //System.out.println("Calling primary Ping");
        PlayerInfo playerInfo = this.authenticate(player, playerId, authCode);
        if (player == null) {
            return IServerReply.PingReply.createUpdate(this.gameState);
        }

        updateLatestAccessTime(player);

        if (isSelf(playerId)) {
            return IServerReply.PingReply.createUpdate(gameState);
        }

        boolean promoted = promotePeerAsBackupIfNeeded(playerId, player);
        if (promoted) {
            return IServerReply.PingReply.createPromoteToBackup(this.gameState, this.serverInternals);
        } else {
            return IServerReply.PingReply.createUpdate(this.gameState);
        }
    }

    /**
     *
     * @param playerId
     * @param player
     * @return
     */
    protected boolean promotePeerAsBackupIfNeeded(String playerId, IPlayer player) {
        if (shouldPromoteBackUp) {
            if (isSelf(playerId)) {
                return false;
            }

            synchronized (this.gameStateLock) {
                if (shouldPromoteBackUp) {
                    shouldPromoteBackUp = false;
                    gameState.getServerconf().setBackupServer(player, playerId);
                    PlayerInfo playerInfo = gameState.getPlayerFromId(playerId);
                    //player.setAlive(true);
                    playerInfo.setStatus(PeerStatus.BACKUP);
                    return true;
                }
            }
        }
        return false;
    }


    /**
     *
     */
    protected IServerReply.MoveReply callPrimaryMove(IPlayer peerPlayer,
                                                     Move.Direction direction,
                                                     String playerId,
                                                     String authCode) {
        //System.out.println("Player is making a move through server.");
        boolean illegalMove = false;
        synchronized (gameStateLock) {
            PlayerInfo playerInfo = authenticate(peerPlayer, playerId, authCode);
            if (player == null) {
                //System.out.println("Player Not Authenticated");
                return IServerReply.MoveReply.createIllegal(gameState);
            }
            //System.out.println("Player authenticated");
            // passed all preliminary checks
            updateLatestAccessTime(player);
            if (direction.equals(Move.Direction.NO_MOVE)){
                System.out.println("Do nothing");
            } else if (direction.equals(Move.Direction.EXIT)) {
                //System.out.println("Player is exiting");
                /* Mark player as dead, that is remove it from the game state */
                gameState.removePlayerFromMaze(playerInfo);
                /* Inform tracker*/
                try {
                    this.player.callTrackerToRemovePlayer(playerInfo.getId());
                } catch (Exception e){
                    //System.out.println("Failed tracker" + e.getMessage());
                }
            } else {
                illegalMove = gameState.processMove(direction, playerId);
            }

            if (!updateBackup()) {
                // failed to update to backup
                // only try to promote if peer is not the primary server
                //System.out.println("Failed to update backup");
                if (!isSelf(playerId)) {
                    System.out.println("Player is not self");
                    if (promotePeerAsBackupIfNeeded(playerId, player)) {
                        System.out.println("Promoting to backup");
                        System.out.println(gameState);
                        return IServerReply.MoveReply.createPromoteToBackup(gameState, serverInternals, illegalMove);
                    }
                }
            } else{
                //System.out.println("Successfully updated backup");
            }

            return IServerReply.MoveReply.createReply(gameState, illegalMove);
        }
    }

    /**
     *
     * @param player
     * @param playerId
     * @param isPrimary
     * @return
     */
    protected IServerReply.AuthReply callPrimaryJoin(IPlayer player,
                                                  String playerId,
                                                  String playerName,
                                                  boolean isPrimary) {
        synchronized (gameStateLock) {
                //System.out.println("Joining a new player");
                // generate player ID and auth code for this player
                String authCode = getAuthCode();
                //System.out.println("AuthCode: " + getAuthCode());
                PlayerInfo playerInfo = new PlayerInfo(playerId,
                                                       playerName,
                                                       null,
                                                       0,
                                                       true,
                                                       PeerStatus.NORMAL);

                // If this is the first peer, it's the primary server
                if (isPrimary) {
                    playerInfo.setStatus(PeerStatus.PRIMARY);
                }
                //System.out.println("Adding player to maze");
                gameState.addPlayerToMaze(playerInfo);
                // update the server secrets
                serverInternals.getAuthCodes().put(playerId, authCode);
                serverInternals.getPeers().put(playerId, player);
                // If this is the second peer, it's the backup server
                boolean becomeBackup = gameState.getPlayersList().size() == 2;
                boolean becomeNormal = gameState.getPlayersList().size() > 2;
                if (becomeBackup) {
                    //System.out.println("Trying to be backup");
                    playerInfo.setStatus(PeerStatus.BACKUP);
                    //System.out.println("Designating server to be backup.");
                    gameState.getServerconf().setBackupServer(player, playerId);
                    System.out.println("Accepted as backup");
                    return new IServerReply.AuthReply(true,
                                                       playerId,
                                                       authCode,
                                                       gameState,
                                                       serverInternals,
                                                       true);
                } else if (becomeNormal){
                    updateBackup();
                    return new IServerReply.AuthReply(true,
                                                       playerId,
                                                       authCode,
                                                       gameState,
                                                       null,
                                                       false);
                }   else {
                    return new IServerReply.AuthReply(true,
                            playerId,
                            authCode,
                            this.gameState,
                            null,
                            false);
                }

        }
    }

    /**
     * A separate Timer Task checks for missing treasures and then calls the relevant function
     * to fill the treasures.
     */
    protected void treasureRefillerTask(){
        //System.out.println("Started Treasure filler task");
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (gameState.checkIfTreasureCountisLessThanK()){
                    //System.out.println("Treasure is less than K.");
                    gameState.refillMazeWithTreasures();
                }
                else{
                    //System.out.println("Treasure is full");
                }
            }
        };
        timer.scheduleAtFixedRate(task, 0, TREASURE_REFILLER_INTERVAL);
    }
}



