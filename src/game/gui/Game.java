package game.gui;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import java.net.InetAddress;
import main.java.Host;
import main.java.PeerPlayer;
import main.java.GameState;
import javafx.application.Platform;

public class Game extends Application {
	
	private static String playerName;
	private static String trackerIp;
	private static String localIp;
	private static int trackerPort;
	private static int localPort;
	private static Host localHost;
	private static Host trackerHost;
	private UIhandler uiHandler;
	private static Stage connectDialog;

	public static void main(String[] args) throws Exception {
		/*
		 * First try to connect with the Tracker
		 * If successful, then announce to join the game
		 */
		if (args.length != 3) throw new Exception("Should be given 3 arguments");
		Platform.runLater(new Runnable() {
			public void run() {
				connectDialog = new Stage();
				TextField connectMsg = new TextField("Connecting to the game pool...");
				connectMsg.setEditable(false);
				connectMsg.setAlignment(Pos.CENTER);
				connectMsg.getStyleClass().add("text-dialog");
				Scene connectScene = new Scene(connectMsg, 300, 100);
				connectScene.getStylesheets().add("game/gui/stylesheet.css");
				connectDialog.setScene(connectScene);
				connectDialog.show();
			}
		});

		trackerIp = args[0];
		trackerPort = Integer.parseInt(args[1]);
		playerName = args[2];
		localIp = InetAddress.getLocalHost().getHostAddress();
		localPort = getPortNumber();
		trackerHost = new Host(trackerIp, trackerPort);
		localHost = new Host(localIp, localPort); // Randomly select an unused port number.
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		/*
		 * Connect to Tracker and handle the remote exception
		 */
		PeerPlayer localPlayer;
		PlayerTable playerTable = new PlayerTable();
		GameGrid grid = new GameGrid();
		
		try {
			localPlayer = new PeerPlayer(trackerHost, this, playerName);
			uiHandler = new UIhandler(localPlayer, primaryStage, playerTable, grid);
			localPlayer.startGame();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception("Cannot resolve the host");
		}
		
		BorderPane mainLayout = new BorderPane();
		playerTable.getTable().setMinSize(300, 300);
		playerTable.getTable().setMaxSize(300, 300);
		grid.setMinSize(300, 300);
		grid.setMaxSize(300, 300);
		mainLayout.setLeft(playerTable.getTable());
		mainLayout.setRight(grid);
		Scene mainScene = new Scene(mainLayout);
		grid.getStyleClass().add("game-grid");
		mainScene.getStylesheets().clear();
		mainScene.getStylesheets().add("game/gui/stylesheet.css");
		mainScene.setOnKeyTyped(e -> uiHandler.changeLoc(e.getCharacter()));
		primaryStage.requestFocus();
		primaryStage.setResizable(false);
		primaryStage.setTitle(playerName);
		primaryStage.setScene(mainScene);
	}
	
	private static int getPortNumber() {
		int MIN_PORT_NUMBER = 20000;
		return MIN_PORT_NUMBER + (int) Math.round(1000 * Math.random());
	}
	
	public void onGameStateChanged(GameState gameState) {
		uiHandler.onGameStateChanged(gameState);
		connectDialog.close();
	}
}
