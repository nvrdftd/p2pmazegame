package main.java;

import java.io.Serializable;

public class Treasure implements Serializable{
    private String treasureId;
    private Location treasureLocation;
    private String assignedPlayer;

    public Treasure(String treasureId,
                    Location treasureLocation,
                    String assignedPlayer){
        this.treasureId = treasureId;
        this.treasureLocation = treasureLocation;
        this.assignedPlayer = assignedPlayer;
    }

    public String getTreasureId(){
        return this.treasureId;
    }

    public Location getTreasureLocation(){
        return this.treasureLocation;
    }

    public String getAssignedPlayer(){
        return this.assignedPlayer;
    }

    public void setTreasureId(String treasureId){
        this.treasureId = treasureId;
    }

    public void setTreasureLocation(Location treasureLocation){
        this.treasureLocation = treasureLocation;
    }

    public void setConsumedBy(String playerId){
        this.assignedPlayer = playerId;
    }

    public String toString(){
        String ret = this.treasureId + "\n" + this.treasureLocation.toString() + "\n" + this.assignedPlayer;
        return ret;
    }

    public static void main(String[] args){
        System.out.println("Testing Treasure class");
        Treasure treasure = new Treasure("1",
                                          new Location(2,3),
                                          "playerId");
        System.out.println("TreasureId: " + treasure.getTreasureId());
        System.out.println("Assigned Player: " + treasure.getAssignedPlayer());
        System.out.println("Get Location: " + treasure.getTreasureLocation());
        treasure.setConsumedBy("123");
        System.out.println("Assigned Player: " + treasure.getAssignedPlayer());

    }

}
