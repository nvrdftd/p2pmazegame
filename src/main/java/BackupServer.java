package main.java;

import java.rmi.RemoteException;

public abstract class BackupServer extends Server {
    protected IPlayer player;
    protected void callUpdateOnBackUp(GameState gameState, ServerInternals serverInternals){
        System.out.println("Get update from Server");
        synchronized (gameStateLock){
            this.gameState = gameState;
            this.serverInternals = serverInternals;
        }

    }

    /**
     * Any player in the game can detect the death of a server, it call alertDeathOfPrimary.
     */
    protected IServerReply.PingReply alertDeathOfPrimary(IPlayer player,
                                                      String playerId,
                                                      String serverAuth,
                                                      IPlayer deadServer){
        System.out.println("Alerting death of primary");
        PlayerInfo playerInfo = this.authenticate(player, playerId, serverAuth);
        System.out.println(playerInfo);
        System.out.println("Authenticating a player");
        if (player != null){
             /* Implements equality for a player in the game */
             System.out.println("Checking whether the deadPlayer is actually primary");
             boolean isDeadPlayerServer = deadServer.equals(this.gameState.getServerconf().getPrimaryServer());
             System.out.println("ISDEADPLAYERPRIMARY: " + isDeadPlayerServer);
             if (isDeadPlayerServer){
                 /* Ping the primary to see if it is indeed dead */
                 IPlayer primaryServer = this.gameState.getServerconf().getPrimaryServer();
                 System.out.println("Starting to ping");
                 try {
                     primaryServer.callPrimaryPing(this.player, this.playerId, this.serverAuth);
                 } catch (Exception e) {
                     System.out.println("Exception occured in while connecting primary" + e.toString());
                     this.setNotRunning(gameState.getServerconf().getPrimaryId());
                     System.out.println("Set Not running");
                     if(!isSelf(playerId)){
                         System.out.println("-------------------Here-----------------------------");
                         System.out.println("ISNULLORNOT: " + playerInfo);
                         playerInfo.setStatus(PeerStatus.PRIMARY);
                         System.out.println("Promoting player as new Primary Server");
                         this.gameState.getServerconf().setPrimaryServer(player, playerId);
                         return IServerReply.PingReply.createPromoteToPrimary(this.gameState, this.serverInternals);
                     }

                 }
             }
        }
        return IServerReply.PingReply.createUpdate(gameState);
    }
}
