package main.java;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Host implements Serializable{
    private String hostIp;
    private int hostPort;

    public Host(String hostIp, int hostPort){
        this.hostIp = hostIp;
        this.hostPort = hostPort;
    }

    public String getHostIp(){
        return this.hostIp;
    }

    public int getHostPort() {
        return this.hostPort;
    }

    public void setHostPort(int hostPort) { this.hostPort = hostPort;}

    @Override
    public boolean equals(java.lang.Object host){
        Host hostReal = (Host) host;
        return hostReal.getHostIp() == hostIp && hostReal.getHostPort() == hostPort;
    }

    @Override
    public int hashCode(){
        return (hostIp + Integer.toString(hostPort)).hashCode();
    }

    public String toString(){
        return this.hostIp + ":" + this.hostPort;
    }

    public static void main(String[] args){
        Host a = new Host("a", 112);
        Host b = new Host("a", 112);
        System.out.println(a.hashCode());
        System.out.println(b.hashCode());
        System.out.println(a.equals(b));

        Map<Host,Integer> h = new HashMap<Host, Integer>();
        h.put(a, 1);
        System.out.println(h.get(b));
    }

}
