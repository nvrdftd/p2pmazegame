package main.java;

import java.rmi.RemoteException;

public class BeginningPrimaryServer extends PrimaryServer{

    private int N;
    private int K;

    public BeginningPrimaryServer(int N, int K, String playerId, String serverAuth, IPlayer player){
        this.N = N;
        this.K = K;
        this.playerId = playerId;
        this.serverAuth = serverAuth;
        this.player = player;
        this.gameState = new GameState(N,K);
        this.serverInternals = new ServerInternals();
    }

    public void start(){
        startGame();
    }

    public void startGame() {
        System.out.println("Setting game status");
        this.gameState.setGameStatus(GameState.GameStatus.GAME_INITIALIZING);
        System.out.println("setting primary server");
        this.gameState.getServerconf().setPrimaryServer(this.player, this.playerId);
        System.out.println("Getting game state lock");
        synchronized (this.gameStateLock) {
            System.out.println("Initializing maze with treasures");
            gameState.initMazeWithTreasures();
            System.out.println("Updating backup");
            updateBackup();
            System.out.println("Starting alive Task Checker");
            startAliveCheckerTimerTask();
            System.out.println("Starting treasure refiller");
            treasureRefillerTask();
            this.gameState.setGameStatus(GameState.GameStatus.GAME_STARTED);
        }
    }


    /*public static void main(String[] args){
        try {
            PeerPlayer player = new PeerPlayer(new Host("1", 2), new Host("2", 3));
            BeginningPrimaryServer bPS = new BeginningPrimaryServer(4,10, "1", "sev1", player);
            bPS.startGame();
            
            PeerPlayer newPlayer = new PeerPlayer(new Host("2", 2), new Host("2", 3));
            bPS.callPrimaryJoin(newPlayer, "2", true);
            PeerPlayer newPlayer2 = new PeerPlayer(new Host("3", 2), new Host("2", 3));
            IServerReply.AuthReply authReply =  bPS.callPrimaryJoin(newPlayer2, "3", false);
            IServerReply.MoveReply moveReply = bPS.callPrimaryMove(newPlayer2, Move.Direction.NORTH, "3", authReply.getServerAuth());
            System.out.println("IsIllegalMove: " + moveReply.isIllegalMove());
        } catch (RemoteException e){

        }
    }*/

}
