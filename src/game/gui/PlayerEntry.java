package game.gui;

import javafx.beans.property.*;

public class PlayerEntry {
	private SimpleStringProperty name;
	private SimpleIntegerProperty score;
	private SimpleStringProperty status;

	public PlayerEntry(String name, int score, String status) {
		this.name = new SimpleStringProperty(name);
		this.score = new SimpleIntegerProperty(score);
		this.status = new SimpleStringProperty(status);
	}

	public String getName() {
		return name.get();
	}

	public int getScore() {
		return score.get();
	}

	public String getStatus() {
		return status.get();
	}

	public void setName(String str) {
		name.set(str);
	}

	public void setScore(int pt) {
		score.set(pt);
	}

	public void setStatus(String st) {
		status.set(st);
	}
}
