package main.java;
import java.io.Serializable;

public class Move implements Serializable{
    public enum Direction {
        NO_MOVE,
        WEST,
        SOUTH,
        EAST,
        NORTH,
        EXIT
    }

    private final Direction direction;

    private final String playerId;

    public Move(Direction direction, String id){
        this.direction = direction;
        this.playerId = id;
    }

    public Direction getDirection(){
        return  this.direction;
    }

    public String getPlayerId(){
        return this.playerId;
    }

    public static void main(String[] args){
        Move move = new Move(Direction.NO_MOVE, "1");
        System.out.println(move.getDirection());
        System.out.println(move.getPlayerId());
    }
}
