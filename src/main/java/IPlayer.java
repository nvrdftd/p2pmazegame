package main.java;

import java.rmi.Remote;
import java.rmi.RemoteException;
import game.gui.Game;

public interface IPlayer extends Remote{

    /**
     *
     */
    boolean isServer() throws RemoteException;

    PeerPlayer.Client getClient() throws RemoteException;

    
    Game getGameObject() throws RemoteException;


    /**
     *
     */
    void setIsServerTrue() throws RemoteException;

    /**
     *
     */
    void callTrackerToRemovePlayer(String deadPlayerId) throws RemoteException;

    /*============== CLIENT METHODS =================*/

    /**
     * Called by the Primary server to the peer to signal that the game is
     * started (after 20 seconds from the first join)
     */
    void callClientGameStarted(GameState gameState) throws RemoteException;


    /*============ PRIMARY SERVER METHODS ===========*/

    /**
     * Called by the peers to the Primary server to register as a player
     */
    IServerReply.AuthReply callPrimaryJoin(IPlayer player, String playerId, String playerName, boolean isServer) throws RemoteException;

    /**
     * Called by the peers to the Primary server to make a game move
     */
    IServerReply.MoveReply callPrimaryMove(IPlayer player,
                                     Move.Direction direction,
                                     String playerId,
                                     String authCode) throws RemoteException;

    /**
     * ping primary server
     */
    IServerReply.PingReply callPrimaryPing(IPlayer peer,
                                     String playerId,
                                     String authCode) throws RemoteException;


    /*============ BACKUP SERVER METHODS =============*/

    /**
     * Called by Primary to Backup server to update the game state
     */
    void callUpdateOnBackUp(GameState gameState, ServerInternals serverInternals) throws RemoteException;

    IServerReply.PingReply alertDeathOfPrimary(IPlayer player,
                                               String playerId,
                                               String authCode,
                                               IPlayer deadPrimary) throws RemoteException;
}
