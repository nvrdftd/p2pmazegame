package main.java;

import java.io.Serializable;
import java.util.*;

public class GameState implements Serializable{
    /* Represents the configuration of primary and backup server */
    private ServerConf serverConf = new ServerConf();

    public enum GameStatus {
        GAME_INITIALIZING,
        GAME_STARTED,
        GAME_OVER
    }

    private GameStatus gameStatus;

    private Map<String, PlayerInfo> playerIdToInfo = new HashMap<String, PlayerInfo>();

    private Map<Location, Treasure> locationToTreasure = new HashMap<Location, Treasure>();

    private boolean started;

    private int N;
    private int K;


    public GameState(int N, int K) {
        this.N = N;
        this.K = K;
    }

    public ServerConf getServerconf() {
        return this.serverConf;
    }

    public boolean isRunning() {
        return started;
    }

    public void setServerConf(ServerConf serverConf) {
        this.serverConf = serverConf;
    }

    public int getRemainingTreasureCount() {
        return locationToTreasure.size();
    }

    public List<Treasure> getTreasureList() {
        return new ArrayList<Treasure>(locationToTreasure.values());
    }

    public List<PlayerInfo> getPlayersList() {
        return new ArrayList<PlayerInfo>(playerIdToInfo.values());
    }

    public boolean checkIfTreasureCountisLessThanK() {
        return this.getRemainingTreasureCount() < this.K;
    }

    public void addPlayerToMaze(PlayerInfo player) {
        List<Location> disallowedLocations = this.getDisallowedLocations();

        Boolean found = false;
        while (!found) {
            Location randomLocation = this.getRandomLocation();
            if (!disallowedLocations.contains(randomLocation)) {
                found = true;
                player.setLoc(randomLocation);
                this.playerIdToInfo.put(player.getId(), player);
            }
        }
    }

    private Location getRandomLocation() {
        Random rand = new Random();
        boolean notExit = true;
        int X = -1;
        int Y = -1;
        while(notExit) {
            X = rand.nextInt(this.N);
            Y = rand.nextInt(this.N);
            if (!this.locationToTreasure.containsKey(new Location(X,Y))) {
                notExit = false;
            }
        }
        return new Location(X, Y);
    }

    public void initMazeWithTreasures() {
        Location location;
        for (int i = 0; i < K; i++) {
            Location newTreasureCoordinate = this.getRandomLocation();
            String treasureId = UUID.randomUUID().toString();
            Treasure treasure = new Treasure(treasureId, newTreasureCoordinate, null);
            this.locationToTreasure.put(newTreasureCoordinate, treasure);
        }
    }

    public void removePlayerFromMaze(PlayerInfo player) {
        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&Removing player from maze&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        String playerId = player.getId();
        if (!this.playerIdToInfo.containsKey(playerId)) {
            System.out.println("Player is not present");
        }
        this.playerIdToInfo.remove(playerId);
    }

    private boolean playerExistsInPosition(int X, int Y) {
        boolean exists = false;
        for (PlayerInfo playerinfo : this.playerIdToInfo.values()) {
            int x = playerinfo.getLoc().getX();
            int y = playerinfo.getLoc().getY();
            if (x == X && y == Y) {
                exists = true;
                break;
            }
        }
        return exists;
    }

    private void getTreasureIfAny(Location loc, PlayerInfo playerInfo) {
        if (this.locationToTreasure.containsKey(loc)) {
            System.out.println("Earned a treasure.");
            Treasure treasure = this.locationToTreasure.get(loc);
            treasure.setConsumedBy(playerInfo.getId());
            playerInfo.setTreasuresEarned(playerInfo.getTreasuresEarned() + 1);
            this.locationToTreasure.remove(loc);
        }
    }

    public boolean processMove(Move.Direction direction, String playerId) {
        PlayerInfo playerInfo = this.playerIdToInfo.get(playerId);
        Location currentLoc = playerInfo.getLoc();

        boolean illegalMove = false;
        switch (direction) {
            case WEST: {
                if (currentLoc.getX() - 1 < 0) {
                    illegalMove = true;
                }
                if (this.playerExistsInPosition(currentLoc.getX() - 1, currentLoc.getY())) {
                    illegalMove = true;
                }

                if (!illegalMove) {
                    currentLoc.setX(currentLoc.getX() - 1);
                    this.getTreasureIfAny(currentLoc, playerInfo);
                }
                break;
            }
            case SOUTH: {
                if (currentLoc.getY() - 1 < 0) {
                    illegalMove = true;
                }
                if (this.playerExistsInPosition(currentLoc.getX(), currentLoc.getY() - 1)) {
                    illegalMove = true;
                }

                if (!illegalMove) {
                    currentLoc.setY(currentLoc.getY() - 1);
                    this.getTreasureIfAny(currentLoc, playerInfo);
                }
                break;
            }
            case EAST: {
                if (currentLoc.getX() + 1 > (N-1)) {
                    illegalMove = true;
                }
                if (this.playerExistsInPosition(currentLoc.getX() + 1, currentLoc.getY())) {
                    illegalMove = true;
                }

                if (!illegalMove) {
                    currentLoc.setX(currentLoc.getX() + 1);
                    this.getTreasureIfAny(currentLoc, playerInfo);
                }
                break;
            }
            case NORTH: {
                if (currentLoc.getY() + 1 > (N - 1)) {
                    illegalMove = true;
                }
                if (this.playerExistsInPosition(currentLoc.getX(), currentLoc.getY() + 1)) {
                    illegalMove = true;
                }

                if (!illegalMove) {
                    currentLoc.setY(currentLoc.getY() + 1);
                    this.getTreasureIfAny(currentLoc, playerInfo);
                }
                break;
            }
            default:{
                break;
            }
        }
        return illegalMove;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public void startGame() {
        this.started = true;
    }

    public List getDisallowedLocations() {
        List<Location> disallowedLocations = new ArrayList<Location>();

        for (Location loc : this.locationToTreasure.keySet()) {
            disallowedLocations.add(loc);
        }

        for (PlayerInfo playerinfo : this.playerIdToInfo.values()) {
            disallowedLocations.add(playerinfo.getLoc());
        }
        return disallowedLocations;
    }

    public void refillMazeWithTreasures() {
        if (this.checkIfTreasureCountisLessThanK()) {
            int numTreasuresToFill = this.K - this.getRemainingTreasureCount();
            List disallowedLocations = this.getDisallowedLocations();
            while (numTreasuresToFill > 0) {
                Location randomLocation = this.getRandomLocation();
                if (!disallowedLocations.contains(randomLocation)) {
                    String treasureId = UUID.randomUUID().toString();
                    Treasure treasure = new Treasure(treasureId, randomLocation, null);
                    this.locationToTreasure.put(randomLocation, treasure);
                    numTreasuresToFill--;
                }

            }
        }
    }

    public PlayerInfo getPlayerFromId(String playerId) {
        return this.playerIdToInfo.get(playerId);
    }

    
    public int getBoardSize() {
    	return N;
    }

    /**
     * GameState functionality testing.
     */
    public static void main(String[] args){
        /* Testing initializing game state.*/
        GameState gameState = new GameState(4, 10);
        gameState.initMazeWithTreasures();
        System.out.println(gameState.getTreasureList().toString());
        /* Testing add a single player*/
        PlayerInfo playerInfo = new PlayerInfo("1", "AA", null, 0,false, PeerStatus.NORMAL);
        gameState.addPlayerToMaze(playerInfo);
        System.out.println("Player Location");
        System.out.println(gameState.getPlayersList().toString());
        /* Process a Move */
        for (int i = 0;i < 10 ;i++) {
            boolean illegalMove = gameState.processMove(Move.Direction.WEST, "1");
            System.out.println("illegalMove: " + illegalMove);
            System.out.println(gameState.getPlayersList().toString());
        }
        System.out.println(gameState.getTreasureList().toString());
        gameState.refillMazeWithTreasures();
        System.out.println(gameState.getTreasureList().toString());

    }

    public String toString(){
        String ret = getPlayersList().toString() + '\n' + getTreasureList().toString();
        return ret;
    }
}
