package main.java;

import java.io.Serializable;

interface ITrackerReq extends Serializable{

    class ConnReq implements ITrackerReq {

        private Host host;
        private String transactionId;

        public ConnReq(Host host,
                       String transactionId) {
            this.host = host;
            this.transactionId = transactionId;
        }

        public Host getHost(){
            return this.host;
        }

        public String getTransactionId(){
            return this.transactionId;
        }

    }

    class AnnounceReq implements  ITrackerReq {

        private Host host;
        private String connectionId;
        private String transactionId;

        public AnnounceReq(Host host,
                            String connectionId,
                            String transactionId){
            this.host = host;
            this.connectionId = connectionId;
            this.transactionId = transactionId;
        }

        public Host getHost(){
            return this.host;
        }

        public  String getConnectionId(){
            return this.connectionId;
        }

        public String getTransactionId(){
            return this.transactionId;
        }
    }

    class ScrapeReq implements  ITrackerReq {

        private Host host;
        private String connectionId;
        private String transactionId;

        public ScrapeReq(Host host,
                          String connectionId,
                          String transactionId){

            this.host = host;
            this.connectionId = connectionId;
            this.transactionId = transactionId;
        }

        public Host getHost(){
            return this.host;
        }

        public String getConnectionId(){
            return this.connectionId;
        }

        public String getTransactionId() {
            return this.transactionId;
        }


    }

    class FlushReq implements  ITrackerReq {

        private Host host;
        private String connectionId;
        private String transactionId;
        private String deadConnectionId;

        public FlushReq(Host host,
                         String connectionId,
                         String transactionId,
                         String deadConnectionId){

            this.host = host;
            this.connectionId = connectionId;
            this.transactionId = transactionId;
            this.deadConnectionId = deadConnectionId;
        }

        public Host getHost(){
            return this.host;
        }

        public String getConnectionId(){
            return this.connectionId;
        }

        public String getTransactionId() {
            return this.transactionId;
        }

        public String getDeadConnectionId(){
            return this.deadConnectionId;
        }
    }
}
