package game.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class PlayerTable {
	
	private ObservableList<PlayerEntry> playerList;
	private TableView<PlayerEntry> playerTable;
	
	public PlayerTable() {
		playerList = FXCollections.observableArrayList();
		playerTable = new TableView<PlayerEntry>(playerList);
		playerTable.setEditable(false);
		playerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		TableColumn<PlayerEntry, String> nameCol = new TableColumn<>("Name");
		TableColumn<PlayerEntry, String> scoreCol = new TableColumn<>("Score");
		TableColumn<PlayerEntry, String> statusCol = new TableColumn<>("Status");
		
		
		nameCol.setCellValueFactory(new PropertyValueFactory<PlayerEntry, String>("name"));
		scoreCol.setCellValueFactory(new PropertyValueFactory<PlayerEntry, String>("score"));
		statusCol.setCellValueFactory(new PropertyValueFactory<PlayerEntry, String>("status"));

		playerTable.getColumns().add(nameCol);
		playerTable.getColumns().add(scoreCol);
		playerTable.getColumns().add(statusCol);
	}
	
	public void addEntry(PlayerEntry entry) {
		if (!playerList.contains(entry)) playerList.add(entry);
	}
	
	public void removeEntry(PlayerEntry entry) {
		if (playerList.contains(entry)) playerList.remove(entry);
	}
	
	public void updateEach(String playerId, int score, String status) {
		PlayerEntry entry = new PlayerEntry(playerId, score, status);
		playerList.add(entry);
	}
	
	public void clearAllEntries() {
		playerList.clear();
	}
	
	public TableView<PlayerEntry> getTable() {
		return playerTable;
	}
	
	
}
