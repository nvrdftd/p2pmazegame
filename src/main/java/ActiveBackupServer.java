package main.java;

/**
 * Represents a backup server that is running
 */
class ActiveBackupServer extends BackupServer {

    public ActiveBackupServer() {
        System.out.println("A new backup server is starting.");
    }

    public ActiveBackupServer(GameState gameState, ServerInternals serverInternals, String playerId) {
        this.playerId = playerId;
        this.gameState = gameState;
        this.serverInternals = serverInternals;
    }

}
