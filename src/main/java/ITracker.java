package main.java;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ITracker extends Remote {
    /* ========== Interface definition for the tracker ================ */

    /*
       Players need to fetch a connectionId before announcing there intent to play the
       game.
    */
    ITrackerResp.ConnResp requestToConnect(ITrackerReq.ConnReq req) throws RemoteException;


    /*
      After a player receives a success while connecting to the tracker, the player then
      makes a request to announce to the tracker that it wants to play a game.
    */
    ITrackerResp.AnnouceResp requestToAnnounc(ITrackerReq.AnnounceReq req) throws RemoteException;

    /*
      A player who has announced once can skip announcing again to get the updated list
      of players in the game. It can instead a scrape request to get some metadata about the
      players, because announcing again and again can be costly.
    */
    ITrackerResp.ScrapeResp requestToScrape(ITrackerReq.ScrapeReq req) throws RemoteException;

    /*
      Server can remove any dead player if it discovers one such player
    */
    ITrackerResp.FlushResp requestToFlushDeadPlayer(ITrackerReq.FlushReq req) throws RemoteException;

    /*

     */
    void basicPing(String connectionId, String transactionId, Host pingingHost) throws RemoteException;

    /*

     */
    void findDeadPlayerTimer() throws RemoteException;
}
