package main.java;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class Tracker extends UnicastRemoteObject implements ITracker{

    /* Port Number */
    private int portNumber;
    /* N -> Size of the maze */
    private int N;
    /* Number of treasures in allowed in the maze */
    private int K;

    /* connectionId to Host Map */
    private Map<String, Host> connIdToHostMap = new HashMap<String, Host>();

    /* hosts currently tracked */
    private Map<Host, Boolean> hosts = new HashMap<Host, Boolean>();

    /* Total number of hosts in the  system. */
    private int numHosts;

    /* Total players joined till now */
    private int totalJoinedTillNow = 0;

    private int PING_INTERVAL= 3000;
    protected Map<String, Long> playerLastAccessTime = new HashMap<String, Long>();

    protected final Object trackerStateLock = new Object();
    protected final Object lastAccessStateLock = new Object();

    public Tracker(int portNumber, int N, int K) throws RemoteException{
        this.portNumber = portNumber;
        this.N = N;
        this.K = K;
    }

    private boolean isHostSame(Host h1, Host h2) {
        return h1.getHostIp().equals(h2.getHostIp()) && h1.getHostPort() == h2.getHostPort();
    }

    private boolean authenticate(String connectionId, Host host) {
        if (this.connIdToHostMap.containsKey(connectionId)) {
            Host storedHost = (Host) this.connIdToHostMap.get(connectionId);
            System.out.println(storedHost);
            return this.isHostSame(storedHost, host);
        } else {
            return false;
        }
    }

    public ITrackerResp.ConnResp requestToConnect(ITrackerReq.ConnReq req) throws RemoteException {
        Host connectingHost = req.getHost();
        String transactionId = req.getTransactionId();
        ITrackerResp.ConnResp resp = null;
        ITrackerResp.ConnResp.PlayerRole playerRole;
        if (!this.hosts.containsKey(connectingHost)) {
            synchronized (trackerStateLock){
                String connectionId = UUID.randomUUID().toString();
                numHosts ++;
                totalJoinedTillNow ++;
                connectingHost.setHostPort(portNumber + totalJoinedTillNow);
                System.out.println(connectingHost);
                System.out.println(connectingHost.hashCode());
                this.connIdToHostMap.put(connectionId, connectingHost);
                this.hosts.put(connectingHost,true);
                if (numHosts == 1){
                    playerRole = ITrackerResp.ConnResp.PlayerRole.SERVER;
                } else {
                    playerRole = ITrackerResp.ConnResp.PlayerRole.NORMAL;
                }

                resp = new ITrackerResp.ConnResp(connectionId,
                                                 transactionId,
                                                 totalJoinedTillNow,
                                                 false,
                                                 null);
                resp.setPlayerRole(playerRole);
                System.out.println("Player successfully connected!");
            }

        }
        else{

            resp = new ITrackerResp.ConnResp(null,
                                              transactionId,
                                             -1,
                                             true,
                                             "Player already connected!");
            System.out.println("Player already connected!");
        }
        return resp;
    }

    public ITrackerResp.AnnouceResp requestToAnnounc(ITrackerReq.AnnounceReq req) throws RemoteException{
        Host announcingHost = req.getHost();
        System.out.println(announcingHost);
        String transactionId = req.getTransactionId();
        String connectionId = req.getConnectionId();
        ITrackerResp.AnnouceResp resp = null;
        if (connectionId == null){
            resp = new ITrackerResp.AnnouceResp(null,
                                                transactionId,
                                               null,
                                               true,
                                               "Invalid Announce Request: Please Connect First");
            System.out.println("Player should announce first!");
        } else if(!this.authenticate(connectionId, announcingHost)) {
            resp = new ITrackerResp.AnnouceResp(null,
                    transactionId,
                    null,
                    true,
                    "Wrong Connection Id for Host");
            System.out.println("Wrong connId provided.");

        } else {
            Map trackerInfo = new HashMap();
            trackerInfo.put("N", this.N);
            trackerInfo.put("K", this.K);
            trackerInfo.put("Players", new ArrayList<Host>(this.hosts.keySet()));
            System.out.println(trackerInfo);
            trackerInfo.put("Total", this.totalJoinedTillNow);
            resp = new ITrackerResp.AnnouceResp(connectionId,
                    transactionId,
                    trackerInfo,
                    false,
                    null);
            System.out.println("Player successfully announced!");
        }
        return resp;
    }

    public ITrackerResp.ScrapeResp requestToScrape(ITrackerReq.ScrapeReq req) throws RemoteException {
        Host scrapingHost = req.getHost();
        String transactionId = req.getTransactionId();
        String connectionId = req.getConnectionId();
        System.out.println(connectionId);
        System.out.println(connIdToHostMap);
        ITrackerResp.ScrapeResp resp = null;
        if (connectionId == null){
            resp = new ITrackerResp.ScrapeResp(null,
                    transactionId,
                    -1,
                    true,
                    "Invalid Announce Request: Please Connect First");
        } else if(!this.authenticate(connectionId, scrapingHost)) {
            resp = new ITrackerResp.ScrapeResp(null,
                    transactionId,
                    -1,
                    true,
                    "Wrong Connection Id for Host");

        } else {
            resp = new ITrackerResp.ScrapeResp(connectionId,
                    transactionId,
                    this.numHosts,
                    false,
                    null);
        }
        return resp;
    }

    public ITrackerResp.FlushResp requestToFlushDeadPlayer(ITrackerReq.FlushReq req) throws RemoteException{
        Host flushingHost = req.getHost();
        String transactionId = req.getTransactionId();
        String connectionId = req.getConnectionId();
        String deadConnectionId = req.getDeadConnectionId();
        ITrackerResp.FlushResp resp = null;
        if (connectionId == null){
            resp = new ITrackerResp.FlushResp(null,
                    transactionId,
                    true,
                    "Invalid FLush Request: Please Connect First");
        } else if(!this.authenticate(connectionId, flushingHost)) {
            resp = new ITrackerResp.FlushResp(null,
                    transactionId,
                    true,
                    "Wrong Connection Id for Host");

        } else {
            System.out.println("Flushing dead player");
            synchronized (trackerStateLock){
                Host hostToFlush = this.connIdToHostMap.get(deadConnectionId);
                this.connIdToHostMap.remove(deadConnectionId);
                System.out.println(hostToFlush);
                System.out.println(this.hosts.get(hostToFlush));
                this.hosts.remove(hostToFlush);
                System.out.println(this.hosts);
                this.numHosts --;
                System.out.println("Flushed dead player");
            }
            resp = new ITrackerResp.FlushResp(connectionId,
                    transactionId,
                    false,
                    null);
        }
        return resp;
    }

    public void basicPing(String connectionId,
                          String transactionId,
                          Host pingingHost) throws RemoteException{
        if (connectionId != null && this.authenticate(connectionId, pingingHost)) {
            System.out.println("doing a ping");
            this.playerLastAccessTime.put(connectionId, System.currentTimeMillis());
            checkAliveStatusOfPlayers();
            System.out.println(this.hosts.keySet());

        }
    }

    public void findDeadPlayerTimer(){
        final Timer timer = new Timer();
        final TimerTask task = new TimerTask() {
            @Override
            public void run() {
                System.out.println("Starting Alive Status checker");
                checkAliveStatusOfPlayers();
            }
        };
        //Change the timer ping intervals.
        timer.scheduleAtFixedRate(task, 0, PING_INTERVAL);
        System.out.println("Alive Checker Time Task Started");
    }

   private void checkAliveStatusOfPlayers(){
        System.out.println("Checking alive status of players");
        Iterator<String> it = this.connIdToHostMap.keySet().iterator();
            while (it.hasNext()) {
                String connId = it.next();
                Long lastAccessTime = this.playerLastAccessTime.get(connId);
                long silentPeriod = System.currentTimeMillis() - lastAccessTime;
                if (silentPeriod > 3 * PING_INTERVAL) {
                    Host host = (Host) this.connIdToHostMap.get(connId);
                    System.out.println(host);
                    this.connIdToHostMap.remove(connId);
                    this.hosts.remove(host);
                    numHosts--;
                    System.out.println("Removing Host" + connId);

                }
            }

    }

    public static void main(String[] args) throws Exception {
        if (args.length < 3){
            throw new Exception("Invalid number of arguments");
        }

        int portToListenOn = Integer.parseInt(args[0]);
        int N = Integer.parseInt(args[1]);
        int K = Integer.parseInt(args[2]);

        try {
            ITracker tracker = new Tracker(portToListenOn, N, K);
            Registry registry = LocateRegistry.createRegistry(portToListenOn);
            registry.rebind("TRACKER", tracker);
            System.out.println(registry.toString());
            System.out.println("Tracker binded to RMI Registry");
            //To be used only if required.
            tracker.findDeadPlayerTimer();
        } catch( Exception e) {
            System.err.println("Error while binding to registry");
            e.printStackTrace();
        }
    }
}
