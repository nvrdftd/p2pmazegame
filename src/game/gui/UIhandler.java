package game.gui;

import javafx.stage.Stage;
import main.java.PeerPlayer;
import main.java.GameState;
import main.java.PeerStatus;
import main.java.Treasure;
import main.java.PlayerInfo;
import main.java.Location;
import main.java.Move.Direction;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import java.util.Optional;
import java.util.List;


class UIhandler {
	
	private PeerPlayer.Client client;
	private Stage stage;
	private PlayerTable table;
	private GameGrid grid;
	private boolean gridInitialized = false;
	List<PlayerInfo> playerList;
	
	public UIhandler(PeerPlayer localPlayer, Stage stage, PlayerTable table, GameGrid grid) {
		this.client = localPlayer.getGameClient();
		this.stage = stage;
		this.table = table;
		this.grid = grid;
		stage.setOnCloseRequest(e -> {
			checkPlayerExit();
			e.consume();
		});
	}
	
	
	public void setPlayerId(String playerId) {
		client.setPlayerId(playerId);
	}
	
	public void changeLoc(String msg) {
		switch (msg) {
		case "0":
			/*
			 * Only update the game state retrieved from the server
			 */
			client.sendMoveAsync(Direction.NO_MOVE);
			break;
		case "1":
			/*
			 * Request to move west on the grid
			 */
			client.sendMoveAsync(Direction.WEST);
			break;
		case "2":
			/*
			 * Request to move south on the grid
			 */
			client.sendMoveAsync(Direction.SOUTH);
			break;
		case "3":
			/*
			 * Request to move east on the grid
			 */
			client.sendMoveAsync(Direction.EAST);
			break;
		case "4":
			/*
			 * Request to move north on the grid
			 */
			client.sendMoveAsync(Direction.NORTH);
			break;
		case "9":
			/*
			 * Request to leave the game and exit
			 */
			checkPlayerExit();
			break;
		}
	}
	
	private boolean isGridInitiated(){
		return this.gridInitialized;
	}
	
	private void setGridIsInitialized(){
		this.gridInitialized = true;
	}
	public void onGameStateChanged(GameState gameState) {
		if (isGridInitiated()) {
			/*
			 * Reveal the locations of the current players and treasures
			 */
			List<Treasure> treasureList = gameState.getTreasureList();
			System.out.println(treasureList);
			List<PlayerInfo> playerList = gameState.getPlayersList();
			System.out.println(playerList);
			grid.clearGrid();
			putTreasureOnGrid(treasureList);
			putPlayerOnGrid(playerList);
			updatePlayerTable(playerList);
		} else {
			/* Initiate the grid */
			playerList = gameState.getPlayersList();
			grid.initGrid(gameState.getBoardSize());
			setGridIsInitialized();
			stage.show();
		}
		
	}
	
	private void putTreasureOnGrid(List<Treasure> treasureList) {
		for (Treasure treasure: treasureList) {
			Location location = treasure.getTreasureLocation();
			grid.setTreasureLocation(location.getX(), location.getY());
		}
	}
	
	private void putPlayerOnGrid(List<PlayerInfo> playerList) {
		for (PlayerInfo player: playerList) {
			Location location = player.getLoc();
			String playerId = player.getPlayerName();
			grid.setPlayerLocation(location.getX(), location.getY(), playerId);
		}
	}
	
	private void updatePlayerTable(List<PlayerInfo> playerList) {
		table.clearAllEntries();
		for (PlayerInfo player: playerList) {
			String playerId = player.getPlayerName();
			String playerStatus = player.getStatus().toString();
			if (playerStatus == "DEAD") continue;
			int treasureEarned = player.getTreasuresEarned();
			System.out.println(playerList);
			table.updateEach(playerId, treasureEarned, playerStatus);
		}
	}
	
	
	
	private void checkPlayerExit() {		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Exit Confirmation");
		alert.setHeaderText("That's it? Are you sure you want to quit?");
		alert.setContentText("Think before act.");

		ButtonType yes = new ButtonType("YES");
		ButtonType no = new ButtonType("NO");

		alert.getButtonTypes().setAll(yes, no);

		alert.showAndWait().ifPresent(response -> {
			if (response == yes) {
				client.sendMoveAsync(Direction.EXIT);
				stage.close();
		    	System.exit(0);
		    }
		});
	}
}
