package main.java;

import java.util.List;
import java.util.UUID;

/**
 * The main P2PGame is divided into some game server related work and a game client related work.
 * The following abstract class Server is inherited by the following kind of servers:
 * 1. PrimaryServers 2. BackUpServers.
 */

public abstract class Server {
    protected GameState gameState;
    protected ServerInternals serverInternals;
    protected final Object gameStateLock = new Object();
    protected String playerId;
    protected String serverAuth;
    protected int PING_INTERVAL = 2000;
    protected int TREASURE_REFILLER_INTERVAL = 1000;

    protected final void setRolePrimary(final String playerId){
        if (this.gameState == null){
            return; /* Not yet time to set PrimaryRole */
        }

        final List<PlayerInfo> playerInfos = this.gameState.getPlayersList();
        for (PlayerInfo playerInfo : playerInfos){
            if (playerInfo.getId() == playerId){
                playerInfo.setStatus(PeerStatus.PRIMARY);
            } else if (playerInfo.getStatus() == PeerStatus.PRIMARY){
                playerInfo.setStatus(PeerStatus.NORMAL);
            }
        }
    }

    protected final void setRoleBackup(final String playerId){
        if (this.gameState == null){
            return; /* Not yet time to set PrimaryRole */
        }

        final List<PlayerInfo> playerInfos = this.gameState.getPlayersList();
        for (PlayerInfo playerInfo : playerInfos){
            if (playerInfo.getId() == playerId){
                playerInfo.setStatus(PeerStatus.BACKUP);
            } else if (playerInfo.getStatus() == PeerStatus.BACKUP){
                playerInfo.setStatus(PeerStatus.NORMAL);
            }
        }
    }

    protected final void setNotRunning(String playerId) {
        for (PlayerInfo player : gameState.getPlayersList()) {
            if (player.getId().equals(playerId)) {
                synchronized (gameStateLock) {
                    player.setRunning(false);
                    player.setStatus(PeerStatus.DEAD);
                    break;
                }
            }
        }
    }

    protected int getNumAlive(){
        int numAlive = 0;
        for (PlayerInfo playerInfo: gameState.getPlayersList()){
            if(playerInfo.isRunning()){
                ++numAlive;
            }
        }
        return numAlive;
    }

    protected  boolean isSelf(String playerId) {
        return playerId.equals(this.playerId);
    }

    /**
     * Obtain and verify the player object from player ID and auth code
     */
    protected PlayerInfo authenticate(IPlayer peer, String playerId, String authCode) {
        System.out.println(this.gameState);
        PlayerInfo player = gameState.getPlayerFromId(playerId);
        System.out.println("Authenticating");
        if (player == null || !serverInternals.auth(peer, playerId, authCode)) {
            // not found
            System.out.println("Or auth null");
            return null;
        }
        System.out.println("Authenticating");
        return player;
    }

    protected String getAuthCode(){
        return UUID.randomUUID().toString();
    }
}
