package main.java;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ServerInternals implements Serializable{


/**
 * This class contains the extra security information that is passed between primary and backup server,
 * as well as passed to a peer who is becoming a new server.
 * <p>
 * Objects of this class should not be passed to non-server peers.
 *
 *
 */
    private final Map<String, String> authCodes = new HashMap<String, String>();

    private final Map<String, IPlayer> players = new HashMap<String, IPlayer>();

    public Map<String, String> getAuthCodes() {
        return authCodes;
    }

    public Map<String, IPlayer> getPeers() {
        return players;
    }

    /**
     * Authenticate the player ID and auth code with the peer session.
     */
    public boolean auth(IPlayer player, String playerId, String serverAuthCode) {
        // verify auth codes
        String correctServerAuthCode = getAuthCodes().get(playerId);
        if (correctServerAuthCode == null || !correctServerAuthCode.equals(serverAuthCode)) {
            return false;
        }

        // verify remote object
        IPlayer savedPeer = this.getPeers().get(playerId);
        if (savedPeer == null || player == null) {
            return false;
        }
        return true;
    }

    /*public static void main(String[] args){
        try {
            ServerInternals serverInternals = new ServerInternals();
            serverInternals.getAuthCodes().put("1", "sv1");
            IPlayer player = new PeerPlayer(new Host("2", 2000), "AA");
            serverInternals.getPeers().put("1", player);
            System.out.println(serverInternals.getAuthCodes());
            System.out.println(serverInternals.getPeers());
            System.out.println(serverInternals.auth(player, "1", "sv1"));
            System.out.println(serverInternals.auth(player, "1", "sv2"));
            System.out.println(serverInternals.auth(player, "2", "sv1"));
        } catch (Exception e){
            System.out.println(e.toString());
        }
    }*/
}
