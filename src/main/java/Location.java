package main.java;
import java.io.Serializable;

public class Location implements Serializable{
    private int xAxis;
    private int yAxis;

    public Location(int xAxis, int yAxis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    public int getX() {
        return this.xAxis;
    }

    public int getY() {
        return this.yAxis;
    }

    public void setX(int xAxis) {
        this.xAxis = xAxis;
    }

    public void setY(int yAxis) {
        //System.out.println("DONE");
        this.yAxis = yAxis;
    }

    public String toString(){
        return "xAxis: " + xAxis + " yAxis: " + yAxis;
    }

    @Override
    public boolean equals(java.lang.Object loc){
        Location location = (Location) loc;
        return location.getX() == xAxis && location.getY() == yAxis;
    }

    @Override
    public int hashCode(){
        return (Integer.toString(xAxis) + Integer.toString(yAxis)).hashCode();
    }

}
