package main.java;
import java.io.Serializable;

public class PlayerInfo implements Serializable{

    /* PlayerInfo represents the required information for the required players
       Important thing to note is that, irrespective of the PeerStatus of a node, every node(Player)
       shall know about all the players position in the system.
    */

    private String id;
    private Location loc;
    private int treasuresEarned;
    private boolean running;
    private PeerStatus  status;
    private String playerName;

    public PlayerInfo(String id,
                      String playerName,
                      Location loc,
                      int treasuresEarned,
                      boolean running,
                      PeerStatus status){
        this.id = id;
        this.playerName = playerName;
        this.loc = loc;
        this.treasuresEarned = treasuresEarned;
        this.running = running;
        this.status = status;
    }

    public String getId(){
        return this.id;
    }

    public Location getLoc(){
        return this.loc;
    }

    public int getTreasuresEarned(){
        return this.treasuresEarned;
    }

    public boolean isRunning(){
        return this.running;
    }

    public String getPlayerName(){
        return this.playerName;
    }

    public PeerStatus getStatus(){
        return this.status;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setLoc(Location loc){
        this.loc = loc;
    }

    public void setTreasuresEarned(int treasuresEarned){
        this.treasuresEarned = treasuresEarned;
    }

    public void setRunning(boolean running){
        this.running = running;
    }

    public void setStatus(PeerStatus status){
        this.status = status;
    }

    public String toString(){
        String ret = id + "\n" + this.loc + "\n" + this.treasuresEarned + "\n" + this.status;
        return ret;
    }

}
