package game.gui;

import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;
import javafx.geometry.Insets;
import javafx.geometry.Pos;


public class GameGrid extends GridPane {

	private Label[][] labelGrid;
	private int gridSize;
	
	public void initGrid(int gridSize) {
		this.gridSize = gridSize;
		labelGrid = new Label[gridSize][gridSize];
		for (int i = 0; i < gridSize; i++) {
			for (int j = 0; j < gridSize; j++) {
				Label cell = new Label("");
				cell.setAlignment(Pos.CENTER);
				cell.setTextAlignment(TextAlignment.CENTER);
				cell.setMinSize(300 / gridSize, 300 / gridSize);
				labelGrid[i][j] = cell;
				System.out.println("Initializingng cells");
				this.add(cell, j, i);
			}
		}
	}
	
	protected void clearGrid() {
		for (int i = 0; i < gridSize; i++) {
			for (int j = 0; j < gridSize; j++) {
				labelGrid[i][j].setText("");
				System.out.println("Clearing cell content");
			}
		}
	}

	
	protected void setTreasureLocation(int j, int i) {
		System.out.println("settreasurelocation");
		labelGrid[gridSize-i-1][j].setText("*");
	}
	
	protected void setPlayerLocation(int j, int i, String playerId) {
		System.out.println("setplayerlocation");
		labelGrid[gridSize-i-1][j].setText(playerId);;
	}
}
