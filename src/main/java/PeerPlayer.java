package main.java;


import com.sun.org.apache.xpath.internal.functions.FuncFalse;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import game.gui.Game;
import javafx.application.Platform;

public class PeerPlayer extends UnicastRemoteObject implements IPlayer  {
    private final int PING_INTERVAL = 3000;
    private final int TRACKER_SLEEP_TIME = 5000;

    public static final String PEER_NAME = "PLAYER";
    public static final String TRACKER_NAME = "TRACKER";
    private RemServer remServer;

    private final Client client = new Client();
    private Game game;
    private Host host;

    private final Host trackerHost;
    private String playerId;
    private Map infoFromTracker;
    private boolean becomeServer = false;
    private PrimaryServer primaryServer = null;
    private BackupServer backupServer = null;
    private boolean isServer = false;
    private String playerName = null;

    private ITracker tracker;
    private final ExecutorService exec = Executors.newCachedThreadPool(runnable -> {
        Thread thread = Executors.defaultThreadFactory().newThread(runnable);
        thread.setDaemon(true);
        return thread;
    });


    public PeerPlayer(Host trackerHost, Game game, String playerName) throws RemoteException{
	    this.host = new Host("127.0.0.1", -1);
	    this.trackerHost = trackerHost;
        this.game = game;
        this.playerName = playerName;

    }

    public void startGame() throws RemoteException, AlreadyBoundException, NotBoundException{
        ITracker tracker = null;
        while(tracker == null) {
            tracker = this.getTrackerRmiStub();
            //System.out.println(tracker);
            if (tracker == null) {
                //System.out.println("Tracker not available, trying after a few seconds");
                this.sleep();
            }
        }
        //System.out.println("Tracker found");
        //System.out.println(tracker);
        setTracker(tracker);
        System.out.println("Setting up connection with tracker");
        if(!this.setUpConnWithTracker()){
            //System.out.println("Failed Connecting with tracker");
            return;
        }
        if(!this.annouceToTracker()){
            //System.out.println("Failed Announcing to tracker");
            return;
        }
        System.out.println("Finished announcing to tracker");
        if (becomeServer){
            System.out.println("Will start as server");
            this.remServer = this.createServer(this, true, host);
            this.primaryServer = new BeginningPrimaryServer(
                    this.getBoardSize(),
                    this.getTreasureCount(),
                    this.playerId,
                    null,
                    PeerPlayer.this
            );
            System.out.println("Server Created");
            System.out.println("Starting Server");
            this.primaryServer.start();
            this.isServer = true;
            System.out.println("Primary Started");
            this.client.connectToPrimaryServer(this,
                                                this.playerId,
                                                this.playerName,
                                                true,
                                                this);
        }
        else {
            this.remServer = this.createServer(this, false, host);
            int numtries = 3;
            while (numtries > 0) {
                System.out.println("Trying again");
                IPlayer anticipatedServer = this.discoverServer();
                if (anticipatedServer == null) {
                    System.out.println("Problem No one ");
                } else {
                    this.client.connectToPrimaryServer(anticipatedServer,
                            this.playerId,
                            this.playerName,
                            false,
                            this);
                }
                System.out.println("Start playing game");
                numtries--;
            }
        }

        /*To be used if required */
        pingToTracker();
    }

    /**
     *
     */
    private void pingToTracker(){
        Timer task = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                String transactionId = getNewTransactionId();
                try {
                    tracker.basicPing(playerId, getNewTransactionId(), host);
                } catch (Exception e){
                    //System.out.println("Failed to Ping.");
                }
            }
        };
        task.scheduleAtFixedRate(timerTask, 0 ,2000);
    }

    /**
     *
     */
    private RemServer createServer(IPlayer player, boolean primary, Host host)
        throws RemoteException, AlreadyBoundException{
        Registry registry = null;
        Random randomizer = new Random();
        int listeningPort = 0;
        boolean retry = false;
        int retryLeft = 10;
        do {
            try {
                listeningPort = this.host.getHostPort();
                registry = LocateRegistry.createRegistry(listeningPort);
                registry.bind(PEER_NAME, player);
                System.out.println("Connection to a port done" + listeningPort);
            } catch (RemoteException e) {
                // the exception may have been caused by the port's unavailability
                retry = true;
                if (retryLeft-- <= 0) {
                    // retry too many times already
                    throw e;
                }
            }
        } while (retry);
        return new RemServer(registry, listeningPort, primary);
    }

    public Client getGameClient() {
        return client;
    }

    /** Every player in the game can act as a Server. This class is to contain the port that the peer listens on,
     * and the registry created on that port, also it stores whether it is actually primary.
     */
    private static class RemServer {

        private final Registry registry;

        private final int listenPort;

        public RemServer(Registry registry, int listenPort, boolean isPrimary) {
            this.registry = registry;
            this.listenPort = listenPort;
        }

        public Registry getRegistry() {
            return registry;
        }

        public int getListenPort() {
            return listenPort;
        }

    }

    private void setTracker(ITracker tracker){
        this.tracker = tracker;
    }

    /**
     * Having a connection with the Tracker is important, to ensure this,
     * if at one go, you fail to achieve a connection, you try again after
     * sleeping for some time.
     */
    private void sleep() {
        try {
            Thread.sleep(TRACKER_SLEEP_TIME);
        } catch (InterruptedException e){
            ;
        }
    }

    public void setIsServerTrue(){
        this.isServer = true;
    }


    /**
     *
     */
    public boolean isServer(){
        return this.isServer;
    }

    /**
     *
     */
    public Client getClient(){
        return this.client;
    }

    /**
     *
     */
    private boolean setUpConnWithTracker(){
        boolean success;
        String transactionId = getNewTransactionId();
        ITrackerReq.ConnReq connReq = new ITrackerReq.ConnReq(this.host, transactionId);
        try {
            //System.out.println("Starting to send connect request");
            ITrackerResp.ConnResp connResp = this.tracker.requestToConnect(connReq);
            //System.out.println("Fetched conn resp");
            if (!connResp.isRejected()){
                setPlayerId(connResp.getConnectionId());
                this.host.setHostPort(trackerHost.getHostPort() + connResp.getHostPort());
                if (connResp.getPlayerRole() == ITrackerResp.ConnResp.PlayerRole.SERVER){
                    this.becomeServer = true;
                }
                success = true;
            } else {
                System.out.println("Error: " + connResp.getRejectReason());
                success = false;
            }
        } catch (RemoteException e){
            e.printStackTrace();
            //System.out.println("Error while connecting to the tracker" + e.getMessage());
            success = false;
        }
        return success;
    }

    /**
     *
     */
    private String getNewTransactionId(){
        return "DUMMY STRING";
    }

    /**
     *
     */
    private void setPlayerId(String playerId){
        //System.out.println("------------------PLAYER_ID--------------------------" + playerId);
        this.playerId = playerId;

    }

    /**
     *
     * @returna
     */
    private boolean annouceToTracker(){
        boolean success;
        String transactionId = getNewTransactionId();
        System.out.println(host);
        ITrackerReq.AnnounceReq announceReq = new ITrackerReq.AnnounceReq(host,
                                                                          playerId,
                                                                          transactionId);
        try{
            ITrackerResp.AnnouceResp announceResp = this.tracker.requestToAnnounc(announceReq);
            if (!announceResp.isRejected()){
                this.infoFromTracker = announceResp.getPlayerInfos();
                success = true;
            } else {
                //System.out.println("Error while announcing: " + announceResp.getRejectReason());
                success = false;
            }
        } catch (RemoteException e){
            //System.out.println("Error while connecting to the tracker");
            success = false;
        }
        return success;
    }


    /**
     * After a player connects to the tracker, then it needs to discover a server. To
     * discover a server, it needs to send a ping information to all the servers and ask if
     * they are starting up as the server. Note, we need to do this only once when a new player
     * joins.
     * Note: We have made sure that the server discovery program can only be run by any player
     * other than the server.
     * @return Returns whether it was successful in finding the server.
     */
    private IPlayer discoverServer(){
        IPlayer server = null;
        List<Host> hosts = (List<Host>) this.infoFromTracker.get("Players");
        System.out.println(hosts);
        if (hosts == null){
            System.out.println("Error: Tracker should at least store the current player info");
        }
        for (int i=0; i < hosts.size(); i++){
            Host playerHost = hosts.get(i);
            if (!playerHost.equals(this.host)){
                /* Get stub for the host */
                System.out.println("Got a diffetent host");
                try {
                    System.out.println(playerHost.getHostIp() + ":" + playerHost.getHostPort());
                    Registry registry = LocateRegistry.getRegistry(playerHost.getHostIp(), playerHost.getHostPort());
                    IPlayer player = (IPlayer)registry.lookup("PLAYER");
                    //System.out.println(player.isServer());
                    if (player.isServer()) {
                        //System.out.println("Server Found");
                        server = player;
                        break;
                    }

                } catch(RemoteException | NotBoundException e){

                }
            }
        }

        return server;
    }

    private ITracker getTrackerRmiStub() {
        //System.out.println("Fetching Tracker Interface");
        try {
            Registry registry = LocateRegistry.getRegistry(trackerHost.getHostIp(), trackerHost.getHostPort());
            ITracker tracker = (ITracker) registry.lookup(PeerPlayer.TRACKER_NAME);
            return tracker;
        } catch (RemoteException | NotBoundException e) {
            //System.out.println("Tracker stub not found");
            return null;
        }
    }

    private int getBoardSize(){
        //System.out.println(this.infoFromTracker);
        return (int)this.infoFromTracker.get("N");
    }

    private int getTreasureCount(){
        return (int)this.infoFromTracker.get("K");
    }

    public void callClientGameStarted(GameState gameState) throws RemoteException{

    }

    public IServerReply.AuthReply callPrimaryJoin(IPlayer player, String playerId, String playerName, boolean isServer) throws RemoteException{
        return primaryServer.callPrimaryJoin(player, playerId, playerName, isServer);
    }
    
    public Game getGameObject(){
    	return this.game;
    }

    public void callTrackerToRemovePlayer(String  deadPlayerId) {
        String transactionId = getNewTransactionId();
        ITrackerReq.FlushReq flushReq = new ITrackerReq.FlushReq(host,
                                                                 playerId,
                                                                 transactionId,
                                                                 deadPlayerId);
        try {
            ITrackerResp.FlushResp flushResp = this.tracker.requestToFlushDeadPlayer(flushReq);
            if (!flushResp.isRejected()){
                //System.out.println("Player Info deleted from tracker");
            }else{
                //System.out.println("Player info failed to delete, this is a grave error");
            }
        } catch (RemoteException e){
            e.printStackTrace();
            //System.out.println("Error");
        }
    }


    public IServerReply.MoveReply callPrimaryMove(IPlayer player,
                                                  Move.Direction direction,
                                                  String playerId,
                                                  String authCode) throws RemoteException{
        return primaryServer.callPrimaryMove(player,
                                             direction,
                                             playerId,
                                             authCode);
    }

    public IServerReply.PingReply callPrimaryPing(IPlayer player,
                                           String playerId,
                                           String authCode) throws RemoteException{
        return primaryServer.callPrimaryPing(player,
                                             playerId,
                                             authCode);
    }

    public void callUpdateOnBackUp(GameState gameState, ServerInternals serverInternals) throws RemoteException{
        System.out.println("Updating backup");
        backupServer.callUpdateOnBackUp(gameState, serverInternals);
    }

    public IServerReply.PingReply alertDeathOfPrimary(IPlayer player,
                                                      String playerId,
                                                      String authCode,
                                                      IPlayer deadPrimary) throws RemoteException {
        //System.out.println("BackupServer: " + backupServer);
        return backupServer.alertDeathOfPrimary(player, playerId, authCode, deadPrimary);
    }



    public class Client {
        private GameState gameState;
        private String playerId;
        private String serverAuth;
        private IPlayer selfPlayer;

        public String getPlayerId() {
            return playerId;
        }

        public void setPlayerId(String playerId) {
            this.playerId = playerId;
        }

        public void setSelf(IPlayer selfPlayer){
            this.selfPlayer = selfPlayer;
        }
        public void setServerAuth(String serverAuth) {
            this.serverAuth = serverAuth;
        }

        private void setGameState(GameState gameState) {
            this.gameState = gameState;
            try {
            	System.out.println(gameState);
            	Game game = (Game)this.selfPlayer.getGameObject();
            	System.out.println(game);
            	Platform.runLater(new Runnable() {
        			@Override
        			public void run() {
        				game.onGameStateChanged(gameState);
        			}
        		});
            } catch (Exception e){
            	e.printStackTrace();
            	System.out.println("***Error**: " + e.getMessage());
            }
        }
        
        public GameState getGameState() {
        	return this.gameState;
        }

        public void callClientGameStarted(GameState gameState) {
            this.gameState = gameState;
            try{
            	Game game = (Game)this.selfPlayer.getGameObject();
            	System.out.println(game);
            	Platform.runLater(new Runnable() {
        			@Override
        			public void run() {
        				game.onGameStateChanged(gameState);
        			}
        		});
            } catch(Exception e) {
            	e.printStackTrace();
            }
            this.checkPulse();
        }

        public boolean connectToPrimaryServer(IPlayer primaryServerPlayer,
                                              String playerId,
                                              String playerName,
                                              boolean isServer,
                                              IPlayer selfPlayer) throws RemoteException, NotBoundException {
            try {
                //System.out.println("Connecting to Primary Server at " + host.getHostIp() + ":" + host.getHostPort());
                IServerReply.AuthReply reply = primaryServerPlayer.callPrimaryJoin(selfPlayer,
                                                                                    playerId,
                                                                                    playerName,
                                                                                    isServer);
                //System.out.println("Connected to primary");
                if (!reply.isAccepted()) {
                    //System.out.println("Request was rejected by Primary Server.");
                    return false;
                }
                setPlayerId(playerId);
                setServerAuth(reply.getServerAuth());
                setSelf(selfPlayer);
                this.callClientGameStarted(reply.getGameState());
                //System.out.println("Player has joined finally!");

                if (reply.isBecomeBackup()) {
                    //System.out.println("Promoted to Backup Server");
                    backupServer = new ActiveBackupServer(reply.getGameState(),
                                       reply.getServerInternals(),
                                       this.playerId);;
                }
                return true;
            } catch (RemoteException e) {
                e.printStackTrace();
                return false;
            }
        }

        public void checkPulse(){
            /* Start a time task */
            Timer scheduledTimer = new Timer();
            TimerTask scheduledTask = new TimerTask() {
                @Override
                public void run() {
                    sendPingToServer();
                }
            };
            scheduledTimer.scheduleAtFixedRate(scheduledTask, 0 , PING_INTERVAL);
        }

        private void sendPingToServer(){
            IPlayer primaryServer = gameState.getServerconf().getPrimaryServer();
            //System.out.println(primaryServer);
            try {
                // contact primary
                IServerReply.PingReply reply = primaryServer.callPrimaryPing(selfPlayer, playerId, serverAuth);
                processServerPingReply(reply);

            } catch (Exception e) {
                //System.out.println(e);
                //System.out.println("Alert Server is down");
                processPrimaryServerFailure(selfPlayer, primaryServer);
            }
        }

        /**
         *
         * @param
         */
        private void processPrimaryServerFailure(IPlayer selfPlayer, IPlayer primaryServer){
            //System.out.println("Contact backup now as server is dead.");
            IPlayer backUpServerPlayer = this.gameState.getServerconf().getBackupServer();
            //System.out.println(backUpServerPlayer);
            try {
                IServerReply.PingReply reply = backUpServerPlayer.alertDeathOfPrimary(selfPlayer,
                                                                                       playerId,
                                                                                       serverAuth,
                                                                                       primaryServer);
                processServerPingReply(reply);
            } catch(RemoteException e){
                //System.out.println("Both server and backup server are dead");
            }
        }

        private void processServerPingReply(IServerReply.PingReply serverReply){
            //System.out.println("Contact backup now");
            IPlayer backUpServerPlayer = this.gameState.getServerconf().getBackupServer();
            switch(serverReply.getPromotionStatus()){
                    case PROMOTED_TO_PRIMARY: {
                        //System.out.println("Promoting to Primary");
                        primaryServer = new InheritingPrimaryServer(serverReply.getGameState(),
                                                                    serverReply.getServerInternals(),
                                                                    this.playerId,
                                                                    PeerPlayer.this,
                                                                    this.serverAuth);
                        try {
                            this.selfPlayer.setIsServerTrue();
                        } catch (RemoteException re){
                            ;
                        }
                        primaryServer.start();
                    }
                    case PROMOTED_TO_BACKUP: {
                        //System.out.println("Promoting to Backup");
                        System.out.println("Activating as backup Server");
                        System.out.println(serverReply.getGameState());
                        backupServer = new ActiveBackupServer(serverReply.getGameState(),
                                                              serverReply.getServerInternals(),
                                                              this.playerId);
                    }
                    default: {
                        System.out.println("NEW GAME STATE: " + serverReply.getGameState().getPlayersList());
                        System.out.println("TREASURES: " + serverReply.getGameState().getTreasureList());
                        setGameState(serverReply.getGameState());
                    }
            }
        }

        public void sendMoveAsync(Move.Direction direction) {
            exec.submit(() -> {
                IPlayer primaryServerPlayer = gameState.getServerconf().getPrimaryServer();

                try {
                    // contact primary
                    IServerReply.MoveReply reply = primaryServerPlayer.callPrimaryMove(PeerPlayer.this,
                                                                                        direction,
                                                                                        playerId,
                                                                                        this.serverAuth);
                    if (reply.isIllegalMove()) {
                        System.out.println("Doing an illegal move");
                    }

                    processServerPingReply(reply);

                } catch (RemoteException e) {
                    processPrimaryServerFailure(selfPlayer, primaryServerPlayer);
                }
            });
        }
    }

//    public static void main(String[] args){
//        //System.out.println("Hello");
//        Random random = new Random();
//        Host trackerHost = new Host("127.0.1.1", 8000);
//        Host host = new Host("127.0.1.1", 8000 + random.nextInt(100));
//        try {
//            PeerPlayer peerPlayer = new PeerPlayer(trackerHost, "AA");
//            peerPlayer.startGame();
//            Scanner scanner = new Scanner(System.in);
//            Client client = peerPlayer.getClient();
//            while (true){
//                System.out.println("Please do a move");
//                String line = scanner.nextLine();
//                int direction = Integer.parseInt(line);
//                if (direction == 0){
//                    client.sendMoveAsync(Move.Direction.NO_MOVE);
//                } else if(direction == 1){
//                    client.sendMoveAsync(Move.Direction.WEST);
//                } else if(direction == 2){
//                    client.sendMoveAsync(Move.Direction.SOUTH);
//                } else if(direction == 3){
//                    client.sendMoveAsync(Move.Direction.EAST);
//                } else if(direction == 4){
//                    client.sendMoveAsync(Move.Direction.NORTH);
//                } else if(direction == 9){
//                    client.sendMoveAsync(Move.Direction.EXIT);
//                    System.exit(0);
//                } else {
//                  System.exit(0);
//                }
//            }
//
//
//        } catch(RemoteException | NotBoundException | AlreadyBoundException e){
//            e.printStackTrace();
//        }
//    }
}
